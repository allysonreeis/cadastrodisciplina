import java.util.ArrayList;

public class ControleDisciplina{

	private ArrayList<Disciplina> listaDisciplinas;

	public ControleDisciplina(){
	
		listaDisciplinas = new 	ArrayList<Disciplina>();
	}

	public void adicionarDisciplinas(Disciplina umaDisciplina) {
		listaDisciplinas.add(umaDisciplina);
	}
	
	public void removerDisciplinas(Disciplina umaDisciplina) {
		listaDisciplinas.remove(umaDisciplina);
	}
	
	public void matricularAluno (Aluno umAluno, Disciplina umaDisciplina) {
		umaDisciplina.adicionar(umAluno);
	}
	
	public void removerMatricula (Aluno umAluno, Disciplina umaDisciplina) {
		umaDisciplina.remover(umAluno);
	}
	
	public Disciplina pesquisarNomeDisciplina(String umNome){
	
		for(Disciplina umaDisciplina : listaDisciplinas){
			if(umaDisciplina.getDisciplina().equalsIgnoreCase(umNome)){
				return umaDisciplina;
			}
		}
		return null;
	}

	public void exibirDisciplinas() {
		for (Disciplina umaDisciplina : listaDisciplinas) {
			System.out.println("Disciplina: " + umaDisciplina.getDisciplina() + " Codigo: " + umaDisciplina.getCodigo());
		}
	}

	public void alunosCadastrados(Disciplina umaDisciplina) {
		umaDisciplina.exibirAlunosCadastrados(umaDisciplina);
	}
	
}
