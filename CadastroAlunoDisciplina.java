import java.io.*;
public class CadastroAlunoDisciplina{


	public static void main(String[] args) throws IOException {
		
			InputStream entradaSistema = System.in;			
			InputStreamReader leitor = new InputStreamReader(entradaSistema);
			BufferedReader leitorEntrada = new BufferedReader(leitor);
			String  entradaTeclado;
			char opcao;

			ControleAluno umControleAluno = new ControleAluno();
			ControleDisciplina umControleDisciplina = new ControleDisciplina();
			Aluno umAluno;
			Disciplina umaDisciplina;
			String umNomeDisciplina;
												
			do {	
				System.out.println("==========================\n");
				System.out.println("1 - Cadastrar Aluno");
				System.out.println("2 - Cadastrar Disciplina ");
				System.out.println("3 - Matricular Aluno");
				System.out.println("4 - Buscar  Aluno ");
				System.out.println("5 - Buscar Disciplina");
				System.out.println("6 - Remover aluno da disciplina");
				System.out.println("7 - Remover Disciplina");
				System.out.println("0 - Sair\n");
				System.out.println("Digite a opção Desejada : ");
				entradaTeclado = leitorEntrada.readLine();
				opcao = entradaTeclado.charAt(0);
				
				switch(opcao){		
				case '1':
					System.out.println("Digite o nome da Aluno: ");
					entradaTeclado = leitorEntrada.readLine();				
					String umNome = entradaTeclado;
				
					System.out.println("Digite a Matricula: ");
					entradaTeclado = leitorEntrada.readLine();				
					String umaMatricula = entradaTeclado;
						
					umAluno = new Aluno(umNome, umaMatricula);
					umControleAluno.criaAluno(umAluno);
				break;
				case '2' :
					System.out.println("Digite o nome da Disciplina: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNomeDisciplina = entradaTeclado;
				
					System.out.println("Digite o codigo: ");
					entradaTeclado = leitorEntrada.readLine();				
					String umCodigo = entradaTeclado;
						
					umaDisciplina = new Disciplina(umNomeDisciplina, umCodigo);
					umControleDisciplina.adicionarDisciplinas(umaDisciplina);
				break;
				case '3' :
					umControleAluno.exibirAlunos();
					umControleDisciplina.exibirDisciplinas();
					System.out.println("Digite o nome da Aluno a ser matriculado: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNome = entradaTeclado;
					umAluno = umControleAluno.pesquisarNomeAluno(umNome);
						
					System.out.println("Digite a disciplina para ser matriculado: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNomeDisciplina = entradaTeclado;
						
					umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNomeDisciplina);
					umControleDisciplina.matricularAluno (umAluno, umaDisciplina);
				break;
				case '4' :
				    umControleAluno.exibirAlunos();

					System.out.println("Digite o nome do aluno: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNome = entradaTeclado;

					umAluno = umControleAluno.pesquisarNomeAluno(umNome);
					System.out.println("Aluno: " + umAluno.getNome() + " Matricula: " + umAluno.getMatricula());
				break;
				case '5' :
					umControleDisciplina.exibirDisciplinas();
					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNomeDisciplina = entradaTeclado;
					umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNomeDisciplina);
					System.out.println("\nDISCIPLINA " + umaDisciplina.getDisciplina() + " CODIGO: " + umaDisciplina.getCodigo() + "\n");
					umControleDisciplina.alunosCadastrados(umaDisciplina);

				break;
				case '6' :
					System.out.println("Digite o nome da Aluno: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNome = entradaTeclado;
					umAluno = umControleAluno.pesquisarNomeAluno(umNome);
						
					System.out.println("Digite a disciplina: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNomeDisciplina = entradaTeclado;
					umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNomeDisciplina);
					umControleDisciplina.removerMatricula(umAluno, umaDisciplina);
				break;
				case '7' :
					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();				
					umNomeDisciplina = entradaTeclado;
					umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNomeDisciplina);
					umControleDisciplina.removerDisciplinas(umaDisciplina); 
				break;
				case '0' : 
					opcao = '0';
				break;
				default: 
					System.out.println("Opção incorreta! Insira uma opcao válida.");
				}
				
			}while(opcao != '0');
}
}
