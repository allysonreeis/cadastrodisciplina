package CadastroDisciplinas;
import java.util.ArrayList;

public class ControleAluno{


	private ArrayList<Aluno> listaAlunos;
	
	
	public ControleAluno (){
		listaAlunos = new 	ArrayList<Aluno>();
	}

	
	public void criaAluno (Aluno novoAluno) {	
		listaAlunos.add(novoAluno);
	}
	
	
	public Aluno pesquisarNomeAluno (String umNome){
	
		for(Aluno umAluno : listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNome)){
				return umAluno;
			}
		}
		return null;
	}

	public void remover (Aluno umAluno) {
		listaAlunos.remove(umAluno);
	}

	public void exibirAlunos() {
		for (Aluno umAluno : listaAlunos) {
			System.out.println ("Nome: " + umAluno.getNome() + " Matricula: " + umAluno.getMatricula());
		}
	}
}
