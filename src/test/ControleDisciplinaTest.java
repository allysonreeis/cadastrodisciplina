package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import CadastroDisciplinas.Aluno;
import CadastroDisciplinas.ControleDisciplina;
import CadastroDisciplinas.Disciplina;

public class ControleDisciplinaTest {
	
	Disciplina umaDisciplina;
	ControleDisciplina umControle;
	Aluno umAluno;
	
	@Before
	public void inciandoObjetos() {
		umaDisciplina = new Disciplina("OO", "222222");
		umControle = new ControleDisciplina();
		umAluno  = new Aluno("Junior", "11/1111111");
		
		umControle.adicionarDisciplinas(umaDisciplina);
		umControle.matricularAluno(umAluno, umaDisciplina);
	}
	
	@Test
	public void AdicionarDisciplinatest() {
		assertEquals(umaDisciplina, umControle.pesquisarNomeDisciplina("OO"));
	}
	
	@Test
	public void RemoverDisciplinasTest () {
		umControle.removerDisciplinas(umaDisciplina);
		assertNull(umControle.pesquisarNomeDisciplina("OO"));
	}

}
