package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import CadastroDisciplinas.Aluno;
import CadastroDisciplinas.ControleAluno;

public class ControleAlunoTest {

	Aluno umAluno;
	ControleAluno umControle;
	
	@Before
	public void iniciandoObjetos()
	{
		umAluno = new Aluno("Junior","11/1111111");
		umControle = new ControleAluno();
		umControle.criaAluno(umAluno);
	}
	
	@Test
	public void pesquisarNomeAlunotest() {
		 assertNotNull(umControle.pesquisarNomeAluno("Junior"));
	}
	
	@Test
	public void removerTest () {
		umControle.remover(umAluno);
		assertNull(umControle.pesquisarNomeAluno("Junior"));
	}

}
